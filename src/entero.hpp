#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

class entero_t{
public:
  int data_;
  
public:
  entero_t();
  entero_t(int);
  entero_t(entero_t&);
  ~entero_t();

  int get() const;
  void set(int);
  
  entero_t& operator = (int);
  entero_t& operator = (entero_t&);
  
  entero_t operator + (int);
  entero_t operator + (entero_t&);
  
  entero_t operator - (int);
  entero_t operator - (entero_t&);
  
  entero_t operator * (int);
  entero_t operator * (entero_t&);
  
  double operator / (int);
  double operator / (entero_t&);
  
  bool operator == (int);
  bool operator == (entero_t&);
  
  bool operator != (int);
  bool operator != (entero_t&);
  
  bool operator > (int);
  bool operator > (entero_t&);
  
  bool operator >= (int);
  bool operator >= (entero_t&);
  
  bool operator < (int);
  bool operator < (entero_t&);
  
  bool operator <= (int);
  bool operator <= (entero_t&);
  
  friend ostream& operator<<(ostream& os,const entero_t&);
};