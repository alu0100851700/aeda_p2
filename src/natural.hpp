#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

class natural_t{
public:
  unsigned int data_;
  
public:
  natural_t();
  natural_t(unsigned int);
  natural_t(const natural_t&);
  ~natural_t();

  unsigned int get() const;
  void set(unsigned int);
  
  natural_t& operator = (unsigned int);
  natural_t& operator = (const natural_t&);
  
  natural_t operator+(unsigned int);
  natural_t operator + (natural_t&);
  
  natural_t operator - (unsigned int);
  natural_t operator - (natural_t&);
  
  natural_t operator * (unsigned int);
  natural_t operator * (natural_t&);
  int operator * (int);
  
  double operator / (int);
  double operator / (natural_t&);
  
  bool operator == (int);
  bool operator == (natural_t&);
  
  bool operator != (int);
  bool operator != (natural_t&);
  
  bool operator > (int);
  bool operator > (natural_t&);
  
  bool operator >= (int);
  bool operator >= (natural_t&);
  
  bool operator < (int);
  bool operator < (natural_t&);
  
  bool operator <= (int);
  bool operator <= (natural_t&);
  
  friend ostream& operator<<(ostream& os,const natural_t&);
};