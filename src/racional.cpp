#include "racional.hpp"

int mcd(int num1, int num2) {
    int mcd = 0;
    int a = std::max(num1, num2);
    int b = std::min(num1, num2);
    do {
        mcd = b;
        b = a%b;
        a = mcd;
    } while(b!=0);
    return mcd;
}


racional_t::racional_t():
	num_(0),
	denom_(0){}

racional_t::racional_t(racional_t& rac):
	num_(rac.get_num()),
	denom_(rac.get_denom()){}

racional_t::racional_t(int num, int denom){

	if(num%denom != 0){
		int m;
		
		do{
			m = mcd(num,denom);
			num = num/m;
			denom = denom/m;
		}while( m > 1);

		if(num<0 && denom<0){
			num_ = abs(num);
			denom_ = abs(denom);
		}
		else if(num<0 || denom<0){
			num_ = -abs(num);
			denom_ = abs(denom);
		}
		else{
			num_ = num;
			denom_ = denom;
		}
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

racional_t::~racional_t(){}

void racional_t::set_num(int num){
	if(num%denom_ != 0){
		int m;
		do{
			m = mcd(num,denom_);
			num = num/m;
			denom_ = denom_/m;
		}while( m != 1);

		num_ = num;
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

void racional_t::set_denom(int denom){
	if(num_%denom != 0){
		int m;
		do{
			m = mcd(num_,denom);
			num_ = num_/m;
			denom = denom/m;
		}while( m != 1);

		if(denom < 0){
			num_ = -num_;
			denom_ = abs(denom);
		}
		else
			denom_ = denom;
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

int racional_t::get_num() const{
	return num_;
}

unsigned int racional_t::get_denom() const{
	return denom_;
}

double racional_t::to_double(){
	return (double)num_/denom_;
}

racional_t& racional_t::operator=(racional_t& data){
	num_ = data.get_num();
	denom_ = data.get_denom();
	return *this;
}

racional_t racional_t::operator+(racional_t& op){
	racional_t aux(num_+op.get_num(), denom_+op.get_denom());
	return aux;
}

racional_t racional_t::operator-(racional_t& op){
	racional_t aux(num_-op.get_num(), denom_-op.get_denom());
	return aux;
}

racional_t racional_t::operator*(racional_t& op){
	racional_t aux(num_*op.get_num(), denom_*op.get_denom());
	return aux;
}

racional_t racional_t::operator/(racional_t& op){
	racional_t aux(num_*op.get_denom(), denom_*op.get_num());
	return aux;
}

bool racional_t::operator==(racional_t& op){
	return (num_ == op.get_num() && denom_ == op.get_denom());
}

bool racional_t::operator!=(racional_t& op){
	return (num_ != op.get_num() || denom_ != op.get_denom());
}

bool racional_t::operator>(racional_t& op){
	return (to_double() - op.to_double() > EPSILON);
}

bool racional_t::operator>=(racional_t& op){
	return (to_double() - op.to_double() > -EPSILON);
}

bool racional_t::operator<(racional_t& op){
	return (to_double() - op.to_double() < -EPSILON);
}

bool racional_t::operator<=(racional_t& op){
	return (to_double()-op.to_double() < EPSILON);
}

/////////////////////////////////////////
ostream& operator<<(ostream& os, const racional_t& n){
	os << n.get_num() << "/" << n.get_denom();
	return os;
}
 