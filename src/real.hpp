#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

class real_t{
public:
  double data_;
  
public:
  real_t();
  real_t(double);
  real_t(real_t&);
  ~real_t();

  double get() const;
  void set(double);
  
  real_t& operator = (double);
  real_t& operator = (real_t&);
  
  real_t operator + (double);
  real_t operator + (real_t&);
  
  real_t operator - (double);
  real_t operator - (real_t&);
  
  real_t operator * (double);
  real_t operator * (real_t&);
  
  double operator / (double);
  double operator / (real_t&);
  
  bool operator == (double);
  bool operator == (real_t&);
  
  bool operator != (double);
  bool operator != (real_t&);
  
  bool operator > (double);
  bool operator > (real_t&);
  
  bool operator >= (double);
  bool operator >= (real_t&);
  
  bool operator < (double);
  bool operator < (real_t&);
  
  bool operator <= (double);
  bool operator <= (real_t&);
  
  friend ostream& operator<<(ostream& os, const real_t&);
};