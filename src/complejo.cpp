#include "complejo.hpp"

//////////////////////////////////////////
// CONSTRUCTORES
/////////////////////////////////////////
complejo_t::complejo_t(void):
	real_(0.0),
	img_(0.0){}

complejo_t::complejo_t(double real, double img):
	real_(real),
	img_(img){}

complejo_t::complejo_t(complejo_t& comp):
	real_(comp.get_real()),
	img_(comp.get_img()){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
complejo_t::~complejo_t(void){}


//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
double complejo_t::get_real(void) const{
	return real_;
}

void complejo_t::set_real(double real){
	real_ = real;
}

double complejo_t::get_img(void) const{
	return img_;
}

void complejo_t::set_img(double img){
	img_ = img;
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
complejo_t& complejo_t::operator=(complejo_t& op){
	real_ = op.get_real();
	img_ = op.get_img();
	return *this;
}

complejo_t complejo_t::operator+(complejo_t& op){
	double real = real_ + op.get_real();
	double img = img_ + op.get_img();
	complejo_t aux(real,img);
	return aux;
}

complejo_t complejo_t::operator-(complejo_t& op){
	double real = real_ - op.get_real();
	double img = img_ - op.get_img();
	complejo_t aux(real,img);
	return aux;
}

complejo_t complejo_t::operator*(complejo_t& op){
	double real = (real_ * op.get_real()) - (img_ * op.get_img());
	double img = (real_ * op.get_img()) + (img_ * op.get_real());
	complejo_t aux(real,img);
	return aux;
}

complejo_t complejo_t::operator/(complejo_t& op){
	double real = ((real_ * op.get_real()) + (img_ * op.get_img())) / ((op.get_real() * op.get_real()) + op.get_img() * op.get_img());
	double img = ((img_ * op.get_real()) - (real_ * op.get_img())) / ((op.get_real() * op.get_real()) + op.get_img() * op.get_img());
	complejo_t aux(real,img);
	return aux;
}

bool complejo_t::operator==(complejo_t& op){
	return (abs(real_-op.get_real()) < EPSILON) && (abs(img_ - op.get_real()) < EPSILON);
}

bool complejo_t::operator!=(complejo_t& op){
	return (abs(real_-op.get_real()) > EPSILON) || (abs(img_ - op.get_real()) > EPSILON);
}

bool complejo_t::operator>(complejo_t& op){
	double mod1 = sqrt((img_ * img_) + (real_ * real_));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) > EPSILON);
}

bool complejo_t::operator>=(complejo_t& op){
	double mod1 = sqrt((img_ * img_) + (real_ * real_));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) > -EPSILON);
}

bool complejo_t::operator<(complejo_t& op){
	double mod1 = sqrt((img_ * img_) + (real_ * real_));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) < -EPSILON);
}

bool complejo_t::operator<=(complejo_t& op){
	double mod1 = sqrt((img_ * img_) + (real_ * real_));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) < EPSILON);
}

ostream& operator<<(ostream& os, const complejo_t& n){
	os << n.get_real();
	if(n.get_img()>EPSILON)
		os << " + "<< n.get_img() << "i";
	else if(n.get_img() < -EPSILON)
		os << " - " << abs(n.get_img()) << "i";
	else
		os << "0i";
	return os;
}
