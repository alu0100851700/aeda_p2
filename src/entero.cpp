#include "entero.hpp"

//////////////////////////////////////////
// CONSTRUCTORES
/////////////////////////////////////////
entero_t::entero_t():
	data_(0){}

entero_t::entero_t(int data):
	data_(data){}

entero_t::entero_t(entero_t& ent):
	data_(ent.get()){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
entero_t::~entero_t(){}

//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
int entero_t::get() const{
 return data_; 
}

void entero_t::set(int data){
 data_ = data; 
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
entero_t& entero_t::operator=(int data){
 data_ = data;
 return *this;
}
entero_t& entero_t::operator=(entero_t& data){
 data_ = data.get();
 return *this;
}

//////////////////////////////////////////
entero_t entero_t::operator+(int op){
	entero_t aux(data_ + op);
	return  aux;
}
entero_t entero_t::operator+(entero_t& op){
	entero_t aux(data_ + op.get());
 	return  aux;
}

//////////////////////////////////////////
entero_t entero_t::operator-(int op){
	entero_t aux(data_ - op);
	return aux;
}
entero_t entero_t::operator-(entero_t& op){
	entero_t aux(data_ - op.get());
	return aux;
}

//////////////////////////////////////////
entero_t entero_t::operator*(int op){
	entero_t aux(data_ * op);
	return aux;
}
entero_t entero_t::operator*(entero_t& op){
	entero_t aux(data_ * op.get());
	return aux;
}

//////////////////////////////////////////
double entero_t::operator/(int op){
	return  (data_ / op);
}
double entero_t::operator/(entero_t& op){
	return  (data_ / op.get());
}

//////////////////////////////////////////
bool entero_t::operator==(int op){
 return (data_ == op); 
}
bool entero_t::operator==(entero_t& op){
 return (data_ == op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator!=(int op){
 return (data_ != op); 
}
bool entero_t::operator!=(entero_t& op){
 return (data_ != op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator>(int op){
 return (data_ > op); 
}
bool entero_t::operator>(entero_t& op){
 return (data_ > op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator>=(int op){
 return (data_ >= op); 
}
bool entero_t::operator>=(entero_t& op){
 return (data_ >= op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator<(int op){
  return (data_ < op);
}
bool entero_t::operator<(entero_t& op){
  return (data_ < op.get());
}

//////////////////////////////////////////
bool entero_t::operator<=(int op){
  return (data_ <= op);
}
bool entero_t::operator<=(entero_t& op){
  return (data_ <= op.get());
}

//////////////////////////////////////////
ostream& operator<<(ostream& os, const entero_t& n){
	os << "Entero: "<< n.get();
	return os;
}