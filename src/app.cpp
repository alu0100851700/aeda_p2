#include <cstring>
#include "natural.hpp"
#include "entero.hpp"
#include "real.hpp"
#include "racional.hpp"
#include "complejo.hpp"
#define TRAZA(t) cout << (#t) << " = " << (t) << endl

using namespace std;

int main(){

	cout << "--- NATURAL ---" << endl;
	natural_t nat1(10);
	TRAZA(nat1);
	natural_t nat2(4);
	TRAZA(nat2);
	natural_t nat3;

	cout << "- Operadores aritméticos -" << endl;
	TRAZA(nat1+nat2);
	TRAZA(nat1-nat2);
	TRAZA(nat1/nat2);
	TRAZA(nat1*nat2);

	cout << "- Operadores de comparación -" << endl;
	TRAZA(nat1==nat2);
	TRAZA(nat1!=nat2);
	TRAZA(nat1>nat2);
	TRAZA(nat1>=nat2);
	TRAZA(nat1<nat2);
	TRAZA(nat1<=nat2);

	cout << "\n--- ENTERO ---" << endl;
	entero_t ent1(-4);
	TRAZA(ent1);
	entero_t ent2(6);
	TRAZA(ent2);

	cout << "- Operadores aritméticos -" << endl;
	TRAZA(ent1+ent2);
	TRAZA(ent1-ent2);
	TRAZA(ent1*ent2);
	TRAZA(ent1/ent2);

	cout << "- Operadores de comparación -" << endl;
	TRAZA(ent1==ent2);
	TRAZA(ent1!=ent2);
	TRAZA(ent1>ent2);
	TRAZA(ent1>=ent2);
	TRAZA(ent1<ent2);
	TRAZA(ent1<=ent2);

	cout << "\n--- REAL ---" << endl;
	real_t real1(-4.6);
	TRAZA(real1);
	real_t real2(6.4);
	TRAZA(real2);

	cout << "- Operadores aritméticos -" << endl;
	TRAZA(real1+real2);
	TRAZA(real1-real2);
	TRAZA(real1*real2);
	TRAZA(real1/real2);

	cout << "- Operadores de comparación -" << endl;
	TRAZA(real1==real2);
	TRAZA(real1!=real2);
	TRAZA(real1>real2);
	TRAZA(real1>=real2);
	TRAZA(real1<real2);
	TRAZA(real1<=real2);

	cout << "\n--- RACIONAL ---" << endl;
	racional_t racional1(-4, 3);
	TRAZA(racional1);
	racional_t racional2(5, 7);
	TRAZA(racional2);

	cout << "- Operadores aritméticos -" << endl;
	TRAZA(racional1+racional2);
	TRAZA(racional1-racional2);
	TRAZA(racional1*racional2);
	TRAZA(racional1/racional2);

	cout << "- Operadores de comparación -" << endl;
	TRAZA(racional1==racional2);
	TRAZA(racional1!=racional2);
	TRAZA(racional1>racional2);
	TRAZA(racional1>=racional2);
	TRAZA(racional1<racional2);
	TRAZA(racional1<=racional2);

	cout << "\n--- COMPLEJO ---" << endl;
	complejo_t complejo1(-4.7, 3.5);
	TRAZA(complejo1);
	complejo_t complejo2(5.3, 7.4);
	TRAZA(complejo2);

	cout << "- Operadores aritméticos -" << endl;
	TRAZA(complejo1+complejo2);
	TRAZA(complejo1-complejo2);
	TRAZA(complejo1*complejo2);
	TRAZA(complejo1/complejo2);

	cout << "- Operadores de comparación -" << endl;
	TRAZA(complejo1==complejo2);
	TRAZA(complejo1!=complejo2);
	TRAZA(complejo1>complejo2);
	TRAZA(complejo1>=complejo2);
	TRAZA(complejo1<complejo2);
	TRAZA(complejo1<=complejo2);

	cout << endl;

	return 0;
}