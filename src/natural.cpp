#include "natural.hpp"

//////////////////////////////////////////
// CONSTRUCTORES
/////////////////////////////////////////
natural_t::natural_t(): 
	data_(0){}

natural_t::natural_t(unsigned int data): 
	data_(data){}

natural_t::natural_t(const natural_t& op):
	data_(op.get()){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
natural_t::~natural_t(){}

//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
unsigned int natural_t::get() const{
 return data_; 
}

void natural_t::set(unsigned int data){
 data_ = data; 
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
natural_t& natural_t::operator=(unsigned int data){
	data_ = data;
	return *this;
}
natural_t& natural_t::operator=(const natural_t& nat){
	data_ = nat.get();
	return *this;
}

/////////////////////////////////////////
natural_t natural_t::operator+(unsigned int op){
	natural_t aux(data_ + op);
	return aux;
}
natural_t natural_t::operator+(natural_t& op){
	natural_t aux(data_ + op.get());
	return aux;
}

/////////////////////////////////////////
natural_t natural_t::operator-(unsigned int op){
	natural_t aux(data_ - op);
	return aux;
}
natural_t natural_t::operator-(natural_t& op){
	natural_t aux(data_ - op.get());
	return aux;
}

/////////////////////////////////////////
natural_t natural_t::operator*(unsigned int op){
	natural_t aux(data_ * op);
	return aux;
}
natural_t natural_t::operator*(natural_t& op){
	natural_t aux(data_ * op.get());
	return aux;
}
int natural_t::operator*(int op){
 return  ((int)data_ * op);
}

/////////////////////////////////////////
double natural_t::operator/(int op){
 return  ((int)data_ / op);
}
double natural_t::operator/(natural_t& op){
 return  (data_ / op.get());
}

/////////////////////////////////////////
bool natural_t::operator==(int op){
 return ((int)data_ == op); 
}
bool natural_t::operator==(natural_t& op){
 return (data_ == op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator!=(int op){
 return ((int)data_ != op); 
}
bool natural_t::operator!=(natural_t& op){
 return (data_ != op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator>(int op){
 return ((int)data_ > op); 
}
bool natural_t::operator>(natural_t& op){
 return (data_ > op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator>=(int op){
 return ((int)data_ >= op); 
}
bool natural_t::operator>=(natural_t& op){
 return (data_ >= op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator<(int op){
  return ((int)data_ < op);
}
bool natural_t::operator<(natural_t& op){
  return (data_ < op.get());
}

/////////////////////////////////////////
bool natural_t::operator<=(int op){
  return ((int)data_ <= op);
}
bool natural_t::operator<=(natural_t& op){
  return (data_ <= op.get());
}

/////////////////////////////////////////
ostream& operator<<(ostream& os, const natural_t& n){
	os << "Natural: " << n.get();
	return os;
}