#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;
#define EPSILON 0.001

class complejo_t{
private:
  double real_;
  double img_;
  
public:
  complejo_t(void);
  complejo_t(double, double);
  complejo_t(complejo_t&);
  ~complejo_t(void);

  double get_real(void) const;
  void set_real(double);
  
  double get_img(void) const;
  void set_img(double);

  complejo_t& operator=(complejo_t&);
  
  complejo_t operator+(complejo_t&);
  
  complejo_t operator-(complejo_t&);
  
  complejo_t operator*(complejo_t&);
  
  complejo_t operator/(complejo_t&);
  
  bool operator==(complejo_t&);
  
  bool operator!=(complejo_t&);
  
  bool operator>(complejo_t&);
  
  bool operator>=(complejo_t&);
  
  bool operator<(complejo_t&);
  
  bool operator<=(complejo_t&);
  
  friend ostream& operator<<(ostream& os, const complejo_t&);
};