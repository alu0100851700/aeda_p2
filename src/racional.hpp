#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;
#define EPSILON 0.001

class racional_t{ // ------- num/denom!=0 ------------
private:
  int num_;
  unsigned int denom_;
  
public:
  racional_t();
  racional_t(racional_t&);
  racional_t(int,int);
  
  ~racional_t();
  
  void set_num(int);
  void set_denom(int);
  
  int get_num() const;
  unsigned int get_denom() const;

  double to_double();

  racional_t& operator=(racional_t&);
  
  racional_t operator+(racional_t&);
  
  racional_t operator-(racional_t&);
  
  racional_t operator*(racional_t&);
  
  racional_t operator/(racional_t&);
  
  bool operator==(racional_t&);
  
  bool operator!=(racional_t&);
  
  bool operator>(racional_t&);
  
  bool operator>=(racional_t&);
  
  bool operator<(racional_t&);
  
  bool operator<=(racional_t&);
  
  friend ostream& operator<<(ostream& os, const racional_t&);
  friend int mcd(int,int);
};